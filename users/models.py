from django.db import models


# 权限
class TbPermission(models.Model):
    name = models.CharField(max_length=64)  # 权限说明
    code_name = models.CharField(max_length=64)  # 代码判断

    class Meta:
        # managed = False
        db_table = 'tb_permission'


# 用户
class TbUser(models.Model):
    mobile = models.CharField(unique=True, max_length=11)  # 手机号
    password = models.CharField(max_length=255)  # 密码
    nickname = models.CharField(max_length=16)  # 昵称
    create_time = models.DateTimeField(auto_now_add=True)  # 创建时间

    class Meta:
        # managed = False
        db_table = 'tb_user'


# 门店
class TbShop(models.Model):
    shop_name = models.CharField(max_length=64)  # 门店名
    user = models.ForeignKey('TbUser', models.DO_NOTHING)  # 关联用户
    create_time = models.DateTimeField(auto_now_add=True)  # 创建时间

    class Meta:
        # managed = False
        db_table = 'tb_shop'


# 员工
class TbStaff(models.Model):
    mobile = models.CharField(max_length=11)  # 手机号
    name = models.CharField(max_length=10)  # 姓名
    password = models.CharField(max_length=255)  # 密码
    create_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店
    permission = models.ManyToManyField(TbPermission)  # 关联权限

    class Meta:
        # managed = False
        db_table = 'tb_staff'


# 会员
class TbMember(models.Model):
    mobile = models.CharField(max_length=11)  # 手机号
    member_name = models.CharField(max_length=16)  # 会员姓名
    sex = models.IntegerField()  # 性别
    create_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    shop = models.ForeignKey('TbShop', models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_member'
