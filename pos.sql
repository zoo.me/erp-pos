/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2018/9/14 19:41:29                           */
/*==============================================================*/


drop database if exists pos;

create database pos default charset utf8;

use pos;

/*==============================================================*/
/* Table: tb_cart                                               */
/*==============================================================*/
create table tb_cart
(
   id                   int not null auto_increment,
   goods_num            int not null default 1,
   goods_id             int not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_combo                                              */
/*==============================================================*/
create table tb_combo
(
   id                   int not null auto_increment,
   combo_name           varchar(16) not null,
   combo_price          varchar(16) not null,
   combo_num            varchar(16) not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_goods                                              */
/*==============================================================*/
create table tb_goods
(
   id                   int not null auto_increment,
   goods_name           varchar(15) not null,
   goods_image          varchar(255) not null,
   goods_unit           varchar(10) not null,
   normal_price         varchar(10) not null,
   menmber_price        varchar(10) not null,
   tip                  varchar(64),
   goods_type_id        int not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_goods_type                                         */
/*==============================================================*/
create table tb_goods_type
(
   id                   int not null auto_increment,
   good_type_name       varchar(15) not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_menber                                             */
/*==============================================================*/
create table tb_menber
(
   id                   int not null auto_increment,
   mobile               varchar(11) not null,
   member_name          varchar(16) not null,
   sex                  int not null,
   create_time          datetime not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_order                                              */
/*==============================================================*/
create table tb_order
(
   id                   int not null auto_increment,
   order_num            varchar(30) not null,
   order_status         int not null,
   create_time          datetime not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_order_goods                                        */
/*==============================================================*/
create table tb_order_goods
(
   id                   int not null auto_increment,
   good_num             int not null default 1,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_permission                                         */
/*==============================================================*/
create table tb_permission
(
   id                   int not null auto_increment,
   name                 varchar(64) not null,
   code_name            varchar(64) not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_shop                                               */
/*==============================================================*/
create table tb_shop
(
   id                   int not null auto_increment,
   shop_name            varchar(64) not null,
   user_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_staff                                              */
/*==============================================================*/
create table tb_staff
(
   id                   int not null auto_increment,
   mobile               varchar(11) not null,
   password             varchar(255) not null,
   shop_id              int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_staff_permission                                   */
/*==============================================================*/
create table tb_staff_permission
(
   id                   int not null auto_increment,
   staff_id             int not null,
   permission_id        int not null,
   primary key (id)
);

/*==============================================================*/
/* Table: tb_user                                               */
/*==============================================================*/
create table tb_user
(
   id                   int not null auto_increment,
   mobile               varchar(11) not null unique,
   password             varchar(255) not null,
   nickname             varchar(16) not null,
   primary key (id)
);

alter table tb_cart add constraint FK_Reference_15 foreign key (goods_id)
      references tb_goods (id) on delete restrict on update restrict;

alter table tb_cart add constraint FK_Reference_8 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_combo add constraint FK_Reference_17 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_goods add constraint FK_Reference_11 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_goods add constraint FK_Reference_14 foreign key (goods_type_id)
      references tb_goods_type (id) on delete restrict on update restrict;

alter table tb_goods_type add constraint FK_Reference_9 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_menber add constraint FK_Reference_6 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_order add constraint FK_Reference_7 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_order_goods add constraint FK_Reference_10 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_shop add constraint FK_Reference_12 foreign key (user_id)
      references tb_user (id) on delete restrict on update restrict;

alter table tb_staff add constraint FK_Reference_16 foreign key (shop_id)
      references tb_shop (id) on delete restrict on update restrict;

alter table tb_staff_permission add constraint FK_Reference_13 foreign key (staff_id)
      references tb_staff (id) on delete restrict on update restrict;

alter table tb_staff_permission add constraint FK_Reference_18 foreign key (permission_id)
      references tb_permission (id) on delete restrict on update restrict;

