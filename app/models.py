# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models

from users.models import TbShop


# 商品类型
class TbGoodsType(models.Model):
    good_type_name = models.CharField(max_length=15)  # 商品类型
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_goods_type'


# 商品
class TbGoods(models.Model):
    goods_name = models.CharField(max_length=15)  # 商品名称
    goods_image = models.CharField(max_length=255)  # 商品图片
    goods_unit = models.CharField(max_length=10)  # 计量单位
    normal_price = models.FloatField()  # 商品价格
    member_price = models.FloatField()  # 会员价
    tip = models.CharField(max_length=64, blank=True, null=True)  # 备注
    goods_type = models.ForeignKey(TbGoodsType, models.DO_NOTHING)  # 关联商品类型
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_goods'


# 套餐
class TbCombo(models.Model):
    combo_name = models.CharField(max_length=16)  # 套餐名称
    combo_price = models.FloatField()  # 套餐价格
    combo_num = models.CharField(max_length=16)  # 套餐中商品数量(手动输入)
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_combo'


# 购物车
class TbCart(models.Model):
    goods_num = models.IntegerField()  # 商品数量
    goods = models.ForeignKey(TbGoods, models.DO_NOTHING)  # 关联商品
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_cart'


# 订单项
class TbOrder(models.Model):
    order_num = models.CharField(max_length=30)  # 订单编号
    order_status = models.IntegerField()  # 订单状态
    create_time = models.DateTimeField(auto_now_add=True)  # 创建时间
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_order'


# 订单商品
class TbOrderGoods(models.Model):
    good_num = models.IntegerField()  # 商品数量
    goods = models.ForeignKey(TbGoods, models.DO_NOTHING)  # 关联商品
    order = models.ForeignKey(TbOrder, models.DO_NOTHING)  # 关联订单
    shop = models.ForeignKey(TbShop, models.DO_NOTHING)  # 关联门店

    class Meta:
        # managed = False
        db_table = 'tb_order_goods'
